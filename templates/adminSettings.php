<?php
$appId = OCA\AlbumHelper\AppInfo\Application::APP_ID;
\OCP\Util::addScript($appId, $appId . '-adminSettings');
// loads the script, which should be attached to the empty div
?>

<div id="albumhelper_admin_settings"></div>
