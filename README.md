<!--
SPDX-FileCopyrightText: Frederik Berg
SPDX-License-Identifier: CC0-1.0
-->

# Nextcloud Album Helper
[![pipeline status](https://gitlab.com/frederikb96/nextcloud-album-helper/badges/main/pipeline.svg)](https://gitlab.com/frederikb96/nextcloud-album-helper/-/commits/main) [![Latest Release](https://gitlab.com/frederikb96/nextcloud-album-helper/-/badges/release.svg)](https://gitlab.com/frederikb96/nextcloud-album-helper/-/releases)

This is a Nextcloud server app, which can be installed by administrators of a Nextcloud instance.

The app provides helping functionalities for photo albums. It can automatically create new albums from a old folder collections. Go to the Nextcloud Settings and click on the Sharing tab and fill in a folder path to start the convertion. There are two modes: 1. Create a new album from the specified folder. 2. Create albums from all subfolders of the specified folder. Shares on folders are automatically transferred to the new albums.

Moreover, there is a new beta feature now, where files within albums which have collaborators, are automatically also shared as read-only files with those collaborators via the default file sharing API. This way the photos also appear as normal photos in the timeline, and in recognize etc.

## Disclaimer
Use this app at your own risk. I have tested the app with my folder setups and tried to address various edge cases like shared files and folders within folders and nested folders but there is no guarantee that it will work for you. I am not responsible for any data loss or other damage caused by the use of this app. If you find any bugs or have feature requests, please open an issue on the [Gitlab page](https://gitlab.com/frederikb96/nextcloud-album-helper/-/issues).

## Install
1. For now, you can only manually install the app and there is no version of this app available in the Nextcloud app store yet.
2. Go to [Releases](https://gitlab.com/frederikb96/nextcloud-album-helper/-/releases) and download the latest release package as a tar archive.
3. Extract the archive 'tar -xf albumhelper*.tar.gz'
4. Copy the content of the extracted folder to a folder named `albumhelper` in your `custom_apps` (maybe different name) directory, which is in your Nextcloud server root directory. 
5. Change the owner of the albumhelper folder to www-data (or whatever user your webserver is running with) `chown -R www-data:www-data albumhelper`
6. In the browser, log in with an admin account to your Nextcloud instance and go to the 'Apps' menu. Now search for `AlbumHelper` and enable the app.
7. Convert your folders to albums via the Sharing section of Nextcloud Settings.