import Vue from 'vue'
// load the component
import AdminSettings from './components/AdminSettings.vue'
// load the translation functions and add them to the Vue instance such that they are available in the component
Vue.mixin({ methods: { t, n } })

const View = Vue.extend(AdminSettings)
// mount the component on a the element with id (which is specified in its php part) when script is executed
new View().$mount('#albumhelper_admin_settings')
