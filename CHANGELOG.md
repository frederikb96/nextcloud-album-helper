# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2023-12-25

### Added

- Function, to automatically share files within albums with collaborators of the album. This way the photos also appear as normal photos in the timeline, and in recognize etc. This is a beta feature, which is automatically enabled.

## [1.1.1] - 2023-12-22

### Added

- Notify user about conversion start via notification.

## [1.1.0] - 2023-12-22

### Changed

- When converting multiple subfolders to albums, and one or multiple albums already exist, the app will now continue to convert the other subfolders to albums. Previously, the app would stop converting subfolders to albums if one or more albums already existed. This is useful if you want to convert multiple subfolders to albums, but some of the subfolders are already converted to albums, which might happen if errors occurred during an earlier conversion.

## [1.0.1] - 2023-12-18

### Fixed

- Check if album is already created, and return error if so.
- Improve error messages for user, now displaying the error message.

## [1.0.0] - 2023-12-18

### Added

- First functional version of the app .