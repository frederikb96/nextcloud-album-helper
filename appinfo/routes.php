<?php
// Connects the url to the controller
// So that vue components can send requests to the backend

// So setAdminConfig will be called when the frontend sends a PUT request to /admin-config or setUserConfig when the frontend sends a PUT request to /user-config
// Function is defined in ConfigController.php
return [
	'routes' => [
		['name' => 'config#setAdminConfig', 'url' => '/admin-config', 'verb' => 'PUT'],
		['name' => 'config#setUserConfig', 'url' => '/user-config', 'verb' => 'PUT'],
		['name' => 'config#convertFolder', 'url' => '/convert-folder', 'verb' => 'PUT'],
		['name' => 'config#convertSubFolders', 'url' => '/convert-subfolders', 'verb' => 'PUT'],
	],
];
