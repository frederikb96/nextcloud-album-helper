<?php
declare(strict_types=1);

namespace OCA\AlbumHelper\Album;

use OCP\Files\File;
use OCP\Files\Folder;
use OCP\Files\Node;
use OCP\Share\IShare;
use OCP\Share\IManager as IShareManager;
use OCA\AlbumHelper\ConfigService as ConfigService;
use OCA\Photos\Album\AlbumMapper as AlbumMapper;
use Psr\Log\LoggerInterface;
use OCA\Circles\CirclesManager as CirclesManager;
use OCA\Photos\Album\AlbumInfo;
use OCP\IUserSession;

class FolderToAlbum{
	/** @var LoggerInterface */
	private LoggerInterface $logger;
	/** @var IShareManager */
	private IShareManager $shareManager;
	/** @var ConfigService */
	private ConfigService $configService;
	/** @var AlbumMapper */
	private AlbumMapper $albumMapper;
	/** @var CirclesManager */
	private CirclesManager $circlesManager;
	/** @var IUserSession */
	private IUserSession $userSession;

	public function __construct(LoggerInterface $logger,
								IShareManager $shareManager,
								ConfigService $configService,
								AlbumMapper $albumMapper,
								IUserSession $userSession,
								CirclesManager $circlesManager) {
		$this->logger = $logger;
		$this->shareManager = $shareManager;
		$this->configService = $configService;
		$this->albumMapper = $albumMapper;
		$this->circlesManager = $circlesManager;
		$this->userSession = $userSession;
	}

	// Function which adds an element to the album
	public function addElementToAlbum(Node $element, AlbumInfo $album): bool {
		// Check if element has correct owner, and skip if not owner of the new album
		if ($element->getOwner()->getUID() !== $album->getUserId()) {
			// Log skipped file
			$this->logger->warning('File owner is not the same as album owner, skipping file ' . 
				$element->getName() .' for album ' . $album->getTitle());
			return false; // Skip if the owner is different
		}

		// Check if element is a file and mime type contains image or video
		if ($element->getType() === File::TYPE_FILE) {
			if (str_contains($element->getMimeType(), 'image') || str_contains($element->getMimeType(), 'video')) {
				// Add file to album
				// Get the owner of the file
				$owner = $element->getOwner();
				// Convert owner to string
				$ownerId = $owner->getUID();
				try {
					$this->albumMapper->addFile($album->getId(), $element->getId(), $ownerId);
				} catch (\Exception $e) {
					$this->logger->error('Exception during adding element ' . $element->getName() . 
						' to album: ' . $album->getTitle() . $e->getMessage());
					// Delete album
					try {
						$this->albumMapper->delete($album->getId());
					} catch (\Exception $e) {
						$this->logger->error('Exception during clean up with album deletion: ' . $e->getMessage());
					}
					// throw exception to stop the conversion
					throw $e;
				}
			} else {
				// Simply skip if the mime type is not an image or video
				$this->logger->warning('File is not an image or video, skipping file ' . $element->getName() . 
					' for album ' . $album->getTitle());
				return false;
			}
		} else {
			// Check if node is a folder
			if (!$element instanceof Folder) {
				$this->logger->error('The node ' . $element->getName() . ' is not a folder, but a ' . $element->getType() . 
					' for album ' . $album->getTitle());
				return false;
			}
			// And now get all elements of this folder and call recursively
			$elements = $element->getDirectoryListing();
			foreach ($elements as $element) {
				// Call addElementToAlbum function
				$this->addElementToAlbum($element, $album);
			}
		}
		return true;
	}

	// returns the created album
	public function convertFolderToAlbum(Folder $folder) : AlbumInfo|null {
		// Check if node is a folder
		if (!$folder instanceof Folder) {
			$this->logger->error('FolderToAlbum: Node is not a folder');
			// throw exception to stop the conversion
			throw new \Exception('FolderToAlbum: Node is not a folder');
		}

		// Get the user id of album owner
		$userId = $folder->getOwner()->getUID();
		
		// Get the folder name
		$albumName = $folder->getName();
		// Check if album already exists
		$existingAlbums = $this->albumMapper->getForUser($userId);
		foreach ($existingAlbums as $existingAlbum) {
			if ($existingAlbum->getTitle() === $albumName) {
				$this->logger->warning('FolderToAlbum: Album already exists with name ' . $albumName . 
				' for user ' . $userId . '.');
				return null;
			}
		}

		// Create new empty album
		$album = $this->albumMapper->create($userId, $albumName);

		// Prepare collaborators array
		$collaborators = [];

		// Include user, group, and circle shares
		$shareTypes = [IShare::TYPE_USER, IShare::TYPE_GROUP, IShare::TYPE_CIRCLE];
	
		foreach ($shareTypes as $shareType) {
			$shares = $this->shareManager->getSharesBy($userId, $shareType, $folder, false, -1);
			foreach ($shares as $share) {
				if ($shareType == IShare::TYPE_CIRCLE) {
					// If it's a circle share, get the circle members
					$circleId = $share->getSharedWith();
					// handle exception if circle does not exist
					try {
						// Retrieve the FederatedUser object for the current user
						try {
							$federatedUser = $this->circlesManager->getLocalFederatedUser($userId);
						} catch (\Exception $e) {
							$this->logger->error('Error retrieving federated user: ' . $e->getMessage());
							throw $e; // or handle the exception as required
						}

						// Start a session for the Circles application with the FederatedUser
						$this->circlesManager->startSession($federatedUser);

						$circle = $this->circlesManager->getCircle($circleId);
					} catch (\Exception $e) {
						$this->logger->error('FolderToAlbum: Not possible to get members from circle ' . 
							$share->getSharedWithDisplayName() . ' for album ' . $albumName . ' . Error: ' . $e->getMessage());
						// Delete album
						try {
							$this->albumMapper->delete($album->getId());
						} catch (\Exception $e) {
							$this->logger->error('FolderToAlbum: Exception during clean up with album deletion: ' . $e->getMessage());
						}
						// throw exception to stop the conversion
						throw $e;
					}
					foreach ($circle->getMembers() as $member) {
						// Add each circle member as a collaborator
						$collaborators[] = [
							'id' => $member->getUserId(),
							'type' => IShare::TYPE_USER // Circle members are added as individual users
						];
					}
				} else {
					// Avoid adding the owner as a collaborator
					if ($share->getSharedWith() !== $userId) {
						$collaborators[] = [
							'id' => $share->getSharedWith(),
							'type' => $shareType
						];
					}
				}
			}
		}
		// Remove duplicates from the collaborators array, might happen if shared with a user and a circle that the user is in
		$collaborators = array_unique($collaborators, SORT_REGULAR);

		// Remove owner from collaborators array
		$collaborators = array_filter($collaborators, function($collaborator) use ($userId) {
			return $collaborator['id'] !== $userId;
		});
	
		// Add collaborators to the album
		if (!empty($collaborators)) {
			try {
				$this->albumMapper->setCollaborators($album->getId(), $collaborators);
			} catch (\Exception $e) {
				$this->logger->error('FolderToAlbum: Exception during adding collaborators to album: ' . $albumName . 
					' . Error: ' . $e->getMessage());
				// Delete album
				try {
					$this->albumMapper->delete($album->getId());
				} catch (\Exception $e) {
					$this->logger->error('FolderToAlbum: Exception during clean up with album deletion: ' . $e->getMessage());
				}
				// Throw exception to stop the conversion
				throw $e;
			}
		}

		// Call addElementToAlbum function
		$this->addElementToAlbum($folder, $album);

		// Return the created album
		return $album;
	}

	// Function which calls the convertFolderToAlbum function for each subfolder of the provided folderpath
	public function convertSubFoldersToAlbum(Folder $folder) {
		// Check if node is a folder
		if (!$folder instanceof Folder) {
			$errorMessage = 'FolderToAlbum: Node is not a folder';
			$this->logger->error($errorMessage);
			throw new \Exception($errorMessage);
		}

		// Call the convertFolderToAlbum function for each subfolder
		$elements = $folder->getDirectoryListing();
		foreach ($elements as $element) {
			// Check if node is a folder
			if (!$element instanceof Folder) {
				$this->logger->info('FolderToAlbum: Node is not a folder with folderPath ' . $folder->getPath() . 
					' and node name ' . $element->getName());
				continue;
			}
			// Check if owner of folder is the same as the owner of the element
			if ($element->getOwner()->getUID() !== $folder->getOwner()->getUID()) {
				$this->logger->info('FolderToAlbum: Owner of folder is not the same as owner of element ' . 
					$element->getPath());
				continue;
			}

			// Call convertFolderToAlbum function
			try {
				$this->convertFolderToAlbum($element);
			} catch (\Exception $e) {
				// If an exception is thrown, log it and break
				$this->logger->error('FolderToAlbum: Exception during conversion of folder ' . $element->getName() . 
					' with folderPath ' . $element->getPath() . 'Error: ' . 
					$e->getMessage());
				// Throw exception to stop the conversion
				throw $e;
			}
		}
	}
}
