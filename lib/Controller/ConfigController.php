<?php
// Inherits from the Controller class and manages the admin and user config values
// It directly access the Nextcloud database functionalities
namespace OCA\AlbumHelper\Controller;

use OCP\IConfig;
use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\Attribute\NoAdminRequired;

use OCA\AlbumHelper\AppInfo\Application;
use OCA\AlbumHelper\Album\FolderToAlbum as FolderToAlbum;
use OCP\IUserSession;
use OCP\Files\IRootFolder;

class ConfigController extends Controller {

	public function __construct(
		string $appName,
		IRequest $request,
		private IConfig $config,
		private ?string $userId,
		private FolderToAlbum $folderToAlbum,
		private IUserSession $userSession,
		private IRootFolder $rootFolder
	) {
		parent::__construct($appName, $request);
	}

	/**
	 * Set admin config values
	 *
	 * @param array $values key/value pairs to store in app config
	 * @return DataResponse
	 */
	public function setAdminConfig(array $values): DataResponse {
		// Set the values in the database and is called from the frontend via the routes.php file
		foreach ($values as $key => $value) {
			$this->config->setAppValue(Application::APP_ID, $key, $value);
		}
		return new DataResponse(['status' => 'success']);
	}

	/**
	 * Start folder to album conversion
	 *
	 * @param string $folderPath folderPath to start conversion from
	 * @return DataResponse
	 */
	#[NoAdminRequired]
	public function convertFolder(string $folderPath): DataResponse {
		try {
			$rootFolder = $this->rootFolder->getUserFolder($this->userId);
			try {
				$folder = $rootFolder->get($folderPath);
			} catch (\Exception $e) {
				throw new \Exception('Folder does not exist');
			}
			if ($folder->getOwner()->getUID() !== $this->userId) {
				throw new \Exception('Folder owner is not the same as user');
			}
	
			$result = $this->folderToAlbum->convertFolderToAlbum($folder);
			// Check if result is null
			if ($result === null) {
				throw new \Exception('Album with that name already exists');
			}

			return new DataResponse(['status' => 'success']);
		} catch (\Exception $e) {
			return new DataResponse(['status' => 'error', 'message' => $e->getMessage()], Http::STATUS_BAD_REQUEST);
		}
	}
	

	/**
	 * Start connversion of subfolders to albums
	 *
	 * @param string $folderPath folderPath to start conversion of subfolders from
	 * @return DataResponse
	 */
	#[NoAdminRequired]
	public function convertSubFolders(string $folderPath): DataResponse {
		try {
			$rootFolder = $this->rootFolder->getUserFolder($this->userId);
			// Get the folder
			try {
				$folder = $rootFolder->get($folderPath);
			} catch (\Exception $e) {
				throw new \Exception('Folder does not exist');
			}
			// Throw error if userId does not match owner of folder
			if ($folder->getOwner()->getUID() !== $this->userId) {
				throw new \Exception('Folder owner is not the same as user');
			}

			$this->folderToAlbum->convertSubFoldersToAlbum($folder);

			return new DataResponse(['status' => 'success']);
		} catch (\Exception $e) {
			return new DataResponse(['status' => 'error', 'message' => $e->getMessage()], Http::STATUS_BAD_REQUEST);
		}
	}

	/**
     * Set user config values
     *
     * @param array $values key/value pairs to store in user config
     * @return DataResponse
     */
	#[NoAdminRequired]
    public function setUserConfig(array $values): DataResponse {
        // Ensure the current user is authorized to modify settings
        if ($this->userId !== null) {
            foreach ($values as $key => $value) {
                // Set the values for the specific user
                $this->config->setUserValue($this->userId, Application::APP_ID, $key, $value);
            }
            return new DataResponse(['status' => 'success']);
        }
        return new DataResponse(['status' => 'error', 'message' => 'Unauthorized'], Http::STATUS_UNAUTHORIZED);
    }
}