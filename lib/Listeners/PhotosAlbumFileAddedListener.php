<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Frederik Berg
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\AlbumHelper\Listeners;

use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCA\Photos\Event\PhotosAlbumFileAddedEvent;
use OCA\Photos\Event\PhotosAlbumFileDeletedEvent;
use OCA\Photos\Event\PhotosAlbumDeletedEvent;
use OCA\Photos\Event\PhotosAlbumCollaboratorEvent;
use OCA\Photos\Album\AlbumMapper;
use OCP\Files\IRootFolder;
use OCP\Share\IManager as IShareManager;
use Psr\Log\LoggerInterface;
use OCP\Files\File;
use OCA\Photos\Album\AlbumFile;

class PhotosAlbumFileAddedListener implements IEventListener {

	public function __construct(
		private AlbumMapper $albumMapper,
		private IRootFolder $rootFolder,
		private IShareManager $shareManager,
		private LoggerInterface $logger) {
	}

	public function handle(Event $event): void {
		// Check if correct event type
		if ($event instanceof PhotosAlbumFileAddedEvent) {
			$this->handleFileAddedEvent($event);
			
		} elseif ($event instanceof PhotosAlbumFileDeletedEvent) {
			
		} elseif ($event instanceof PhotosAlbumDeletedEvent) {
			
		} elseif ($event instanceof PhotosAlbumCollaboratorEvent) {
			$this->handleCollaboratorEvent($event);
		}
		return;
	}

	private function handleFileAddedEvent(PhotosAlbumFileAddedEvent $event): void {
        $albumId = $event->getAlbumId();
        $fileId = $event->getFileId();
        $owner = $event->getOwner();

        $this->shareAlbumFileWithCollaborators($albumId, $fileId, $owner);
    }

	private function handleCollaboratorEvent(PhotosAlbumCollaboratorEvent $event): void {
        $albumId = $event->getAlbumId();
        $collaboratorsAdded = $event->getCollaboratorsAdded();

        $this->shareAlbumWithNewCollaborators($albumId, $collaboratorsAdded);
    }

	private function shareAlbumFileWithCollaborators(int $albumId, int $fileId, string $owner): void {
        $collaborators = $this->albumMapper->getCollaborators($albumId);
        if (empty($collaborators)) {
            return;
        }

        $albumFile = $this->albumMapper->getForAlbumIdAndFileId($albumId, $fileId);
        if (!$albumFile) {
            $this->logger->warning("Album file not found for albumId {$albumId} and fileId {$fileId}");
            return;
        }

        $ownerFile = $albumFile->getOwner();
        if ($ownerFile !== $owner) {
            $this->logger->warning("Owner mismatch for album file in albumId {$albumId}");
        }

        foreach ($collaborators as $collaborator) {
            $this->createOrUpdateShare($albumFile, $collaborator['id'], $collaborator['type'], $albumId);
        }
    }

	private function shareAlbumWithNewCollaborators(int $albumId, array $collaboratorsAdded): void {
        // Get all collaborators of the album
        $collaborators = $this->albumMapper->getCollaborators($albumId);

		// Get owner of the album
		$owner = $this->albumMapper->get($albumId)->getUserId();

		// TODO: the owner of the group also, so add to the array
		$collaborators[] = ['id' => $owner, 'type' => 0];
        foreach ($collaborators as $collaborator) {
            $userId = $collaborator['id'];
            // Get albumFiles owned by the collaborator in the album
            $albumFiles = $this->albumMapper->getForAlbumIdAndUserWithFiles($albumId, $userId);
            foreach ($albumFiles as $albumFile) {
                // Share each albumFile with new collaborators
                foreach ($collaboratorsAdded as $newCollaborator) {
					$collaboratorId = $newCollaborator['id'];
               		$collaboratorType = $newCollaborator['type'];
                    $this->createOrUpdateShare($albumFile, $collaboratorId, $collaboratorType, $albumId);
                }
            }
        }
    }

    private function createOrUpdateShare(AlbumFile $albumFile, string $collaboratorId, int $collaboratorType, int $albumId): void {
        // Get owner
		$owner = $albumFile->getOwner();
		// Get real filesystem file
		$file = $this->rootFolder->getUserFolder($owner)->getById($albumFile->getFileId())[0] ?? null;
        if (!$file) {
            $this->logger->error("File file not found in user folder for owner {$owner}");
            return;
        }
		
		// Create Share Note text
        $albumName = $this->albumMapper->get($albumId)->getTitle();
        $shareNote = '{{{Shared via album: ---' . $albumName . '---' . $albumId . '---}}}';
		try {
			$existingShares = $this->shareManager->getSharesBy($owner, $collaboratorType, $file, false, -1);
			$alreadyShared = false;
		}
		catch (\Exception $e) {
			$this->logger->error('Exception during share retrieval: ' . $e->getMessage());
			return;
		}

        foreach ($existingShares as $existingShare) {
            if ($existingShare->getSharedWith() === $collaboratorId) {
                $alreadyShared = true;
                // Append a note to the existing share
                try {
                    $existingShare->setNote($existingShare->getNote() . $shareNote);
                    $this->shareManager->updateShare($existingShare);
                } catch (\Exception $e) {
                    $this->logger->error('Exception during share update: ' . $e->getMessage());
                }
            }
        }
        if ($alreadyShared) {
            return;
        }

        // Create a new share if it doesn't exist
        try {
            $share = $this->shareManager->newShare()
                ->setSharedWith($collaboratorId)
                ->setShareType($collaboratorType)
                ->setSharedBy($owner)
                ->setShareOwner($owner)
                ->setNode($file)
                ->setPermissions(\OCP\Constants::PERMISSION_READ)
                ->setNote($shareNote);
            $this->shareManager->createShare($share);
        } catch (\Exception $e) {
            $this->logger->error('Exception during share creation: ' . $e->getMessage());
        }
    }
}
