<?php
// This is the file which is used to display the settings page in the admin panel
namespace OCA\AlbumHelper\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\Settings\ISettings;
use OCA\AlbumHelper\ConfigService;
use OCA\AlbumHelper\AppInfo\Application;

class Admin implements ISettings {

	public function __construct(
		private ConfigService $configService,
		private IInitialState $initialStateService
	) {
	}

	// Mandadory method for ISettings, sets the default values and returns the template form
	/**
	 * @inheritDoc
	 */
	public function getForm(): TemplateResponse {
		// Global settings which are used every time the settings page is loaded,
		// first time it is set to the default values
		// It uses the ConfigService to get the values from the database where the default values are initially stored
		$appStatus = $this->configService->getAppStatus();

		$adminConfig = [
			'appStatus' => $appStatus
		];
		$this->initialStateService->provideInitialState('admin-config', $adminConfig);

		// Create a new TemplateResponse, pass the app-id and the template file name
		return new TemplateResponse(Application::APP_ID, 'adminSettings'); 
	}

	// Get the section that those app settings should be displayed in
	/**
	 * @inheritDoc
	 */
	public function getSection(): string {
		return 'sharing';
	}

	// Where to place the content on the page
	/**
	 * @inheritDoc
	 */
	public function getPriority(): int {
		return 10;
	}
}
