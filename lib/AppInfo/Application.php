<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Frederik Berg
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\AlbumHelper\AppInfo;

use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCA\Photos\Event\PhotosAlbumFileAddedEvent;
use OCA\Photos\Event\PhotosAlbumFileDeletedEvent;
use OCA\Photos\Event\PhotosAlbumDeletedEvent;
use OCA\Photos\Event\PhotosAlbumCollaboratorEvent;
use OCA\AlbumHelper\Listeners\PhotosAlbumFileAddedListener;

class Application extends App implements IBootstrap {
	public const APP_ID = 'albumhelper';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}

	public function register(IRegistrationContext $context): void {
		$context->registerEventListener(PhotosAlbumFileAddedEvent::class, PhotosAlbumFileAddedListener::class);
		$context->registerEventListener(PhotosAlbumFileDeletedEvent::class, PhotosAlbumFileAddedListener::class);
		$context->registerEventListener(PhotosAlbumDeletedEvent::class, PhotosAlbumFileAddedListener::class);
		$context->registerEventListener(PhotosAlbumCollaboratorEvent::class, PhotosAlbumFileAddedListener::class);
	}

    public function boot(IBootContext $context): void {
	}
}
