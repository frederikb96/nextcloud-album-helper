<?php
// This file is used to read the config values from the database which nextcloud provides
// So it basically provides helper functions
// ConfigController.php stores the values in the database and this file reads them
namespace OCA\AlbumHelper;

use OCP\IConfig;
use OCA\AlbumHelper\AppInfo\Application;

class ConfigService {

    const APP_ID = Application::APP_ID;

    private $config;

    public function __construct(IConfig $config) {
        $this->config = $config;
    }

    public function getAppStatus(): bool {
        // Simply get the value which is either 0 or 1 since the config value is a checkbox which is either checked or not and return true if it is 1
        return $this->config->getAppValue(self::APP_ID, 'appStatus', "0") === '1';
    }
    
}
