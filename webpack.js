const path = require('path')
const webpackConfig = require('@nextcloud/webpack-vue-config') // importing a npm package, in root, dependency of the app

const buildMode = process.env.NODE_ENV
const isDev = buildMode === 'development'
webpackConfig.devtool = isDev ? 'cheap-source-map' : 'source-map'

webpackConfig.stats = {
	colors: true,
	modules: false,
}

const appId = 'albumhelper'
webpackConfig.entry = {
	adminSettings: { import: path.join(__dirname, 'src', 'adminSettings.js'), filename: appId + '-adminSettings.js' },
	userSettings: { import: path.join(__dirname, 'src', 'userSettings.js'), filename: appId + '-userSettings.js' },
}

module.exports = webpackConfig
